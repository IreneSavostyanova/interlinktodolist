package com.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ToDoListController {

    private final ToDoListService toDoListService;

    @Autowired
    public ToDoListController(ToDoListService toDoListService) {
        this.toDoListService = toDoListService;
    }

    @GetMapping("/to_do_list")
    public String showPage(Model model) {
        model.addAttribute("toDoList", new ToDoList());
        model.addAttribute("task", new Task());
        model.addAttribute("lists", toDoListService.getLists());

        return "main";
    }

    @PostMapping("/to_do_list")
    public String createNewList(@ModelAttribute("toDoList") ToDoList toDoList) {
        toDoListService.createToDoList(toDoList);

        return "redirect:/to_do_list";
    }

    @PostMapping("/addTasks")
    public String addTask(@ModelAttribute("task") Task task) {
        toDoListService.addTaskToList(task);

        return "redirect:/to_do_list";
    }

    @ResponseBody
    @GetMapping("/get_tasks")
    public List<Task> getTasksFromList(@RequestParam() int id) {
        return toDoListService.getTasksFromList(id);
    }

    @PostMapping("/mark_task")
    @ResponseBody
    public void markTask(@RequestParam int id) {
        toDoListService.markTaskLikeDone(id);
    }

    @PostMapping("/delete_list")
    public String deleteList(@RequestParam int id) {
        toDoListService.deleteList(id);
        return "redirect:/to_do_list";
    }
}
