package com.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ToDoListRepository {

    private static final String SQL_CREATE_LIST = "INSERT INTO to_do_list (name) values (:name)";
    private static final String SQL_CREATE_TASK = "INSERT INTO task (description, list_id) VALUES (:description, :list_id)";
    private static final String SELECT_ALL_LISTS = "SELECT * from to_do_list";
    private static final String SQL_GET_TASKS_FOR_LIST = "SELECT * FROM task WHERE list_id = :list_id";
    private static final String SQL_MARK_TASK = "UPDATE task SET done = :done where id = :id";
    private static final String SQL_DELETE_LIST = "DELETE FROM to_do_list where id = :id";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public ToDoListRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void addTaskToList(Task task) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("list_id", task.getListId())
                .addValue("description", task.getDescription());

        jdbcTemplate.update(SQL_CREATE_TASK, map);
    }

    public void createToDoList(ToDoList toDoList) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("name", toDoList.getName());
        
        jdbcTemplate.update(SQL_CREATE_LIST, map);
    }

    public void markTaskLikeDone(int taskId) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", taskId)
                .addValue("done", true);

        jdbcTemplate.update(SQL_MARK_TASK, map);
    }

    public List<ToDoList> getLists() {
        return jdbcTemplate.query(SELECT_ALL_LISTS, (rs, rowNum) -> {
            ToDoList toDoList = new ToDoList();
            toDoList.setId(rs.getInt("id"));
            toDoList.setName(rs.getString("name"));

            return toDoList;
        });
    }

    public List<Task> getTasksFromList(int listId) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("list_id", listId);

        return jdbcTemplate.query(SQL_GET_TASKS_FOR_LIST, map, (rs, rowNum) -> {
            Task task = new Task();
            task.setId(rs.getInt("id"));
            task.setDescription(rs.getString("description"));
            task.setDone(rs.getBoolean("done"));
            task.setListId(rs.getInt("list_id"));

            return task;
        });
    }

    public void deleteList(int listId) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", listId);

        jdbcTemplate.update(SQL_DELETE_LIST, map);
    }
}
