package com.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToDoListService {

    private final ToDoListRepository toDoListRepository;

    @Autowired
    public ToDoListService(ToDoListRepository toDoListRepository) {
        this.toDoListRepository = toDoListRepository;
    }

    public void addTaskToList(Task task) {
        toDoListRepository.addTaskToList(task);
    }

    public void createToDoList(ToDoList toDoList) {
        toDoListRepository.createToDoList(toDoList);
    }

    public List<Task> getTasksFromList(int toDoListId) {
        return toDoListRepository.getTasksFromList(toDoListId);
    }

    public List<ToDoList> getLists() {
        return toDoListRepository.getLists();
    }

    public void markTaskLikeDone(int taskId) {
        toDoListRepository.markTaskLikeDone(taskId);
    }

    public void deleteList(int listId) {
        toDoListRepository.deleteList(listId);
    }
}
