-- auto-generated definition
create table to_do_list
(
  id   serial not null
    constraint to_do_list_pk
      primary key,
  name varchar
);

alter table to_do_list
  owner to postgres;

create unique index to_do_list_id_uindex
  on to_do_list (id);

