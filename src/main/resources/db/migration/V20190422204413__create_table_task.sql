-- auto-generated definition
create table task
(
  id          serial not null
    constraint task_pk
      primary key,
  description varchar,
  list_id     integer
    constraint task_to_do_list_id_fk
      references to_do_list
      on update cascade on delete cascade,
  done        boolean
);

alter table task
  owner to postgres;

create unique index task_id_uindex
  on task (id);

